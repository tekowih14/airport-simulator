package airport;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import person.CabinCrewMember;
import person.Passenger;
import person.Person;
import person.Pilot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static flight.CargoFlight.CARGO_LOADED;
import static flight.Flight.*;
import static flight.PassengerFlight.PASSENGER_ON_BOARD;

/**
 * Created by grazi on 28.04.16.
 */
public class Airport {

    private final int capacity;
    private List<Flight> flights = new ArrayList<Flight>();
    private List<Person> persons = new ArrayList<Person>();
    private String location;

    public Airport(String location, int capacity) {
        this.location = location;
        this.capacity = capacity;
    }

    public void acceptNewBuildedFlight(Flight airplane) {

    }

    public void boardingFlightCrew(int flightNumber) {


    }

    public List<Integer> getFlightsWithoutFlightCrew() {
        return null;
    }

    private List<Integer> getFlightsWithCargoStatus (int cargoStatus) {
        return null;
    }

    public List<Integer> getCargoFlights4LoadCargo() {
        return null;
    }

    public void loadCargo4Flight(Integer flightNumber) {

    }

    public List<Integer> getPassengerFlights4LoadPassenger() {
        return null;
    }

    public void loadPassenger4Flight(Integer flightNumber, int anzPassenger) {

    }

    public List<Integer> getFlights4TakeOff() {

        return null;
    }

    public void takeOffFlight(Integer flightNumber) {

    }

    private boolean request4Tracking(Flight flight) {

        return false;
    }

    public List<Integer> getFlightsWithoutDestination() {

        return null;
    }

    public void setDestination(Integer flightNumber, Airport destAirport) {

    }

    public List<Integer> getFlights4Landing() {
        return null;

    }

    public void landingFlight(Integer flightNumber) {

    }

    public List<Integer> getFlights4GoToGate() {

        return null;
    }

    public void unloadPassengers(Integer flightNumber) {

    }

    public List<Integer> getFlights4GoToUnloadCargo() {

        return null;
    }

    public void unloadCargo(Integer flightNumber) {

    }

    public List<Integer> getFlights4GoToUnloadFlightCrew() {

        return null;
    }

    public void unloadFlightCrew(Integer flightNumber) {

    }

    public void acceptNewPilot(Pilot pilot) {


    }

    public void acceptNewCabinCrewMember(CabinCrewMember cabinCrewMember) {

    }

    public void acceptNewPassenger(Passenger passenger) {

    }

    List<Flight> getFlights() {
        return flights;
    }

    List<Pilot> getPilots() {
        return null;
    }

    List<CabinCrewMember> getCabinCrewMembers() {
        return null;
    }

    List<Passenger> getPassengers() {
        return null;
    }


    public String getLocation() {
        return location;
    }
}
