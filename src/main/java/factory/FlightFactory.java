package factory;

import flight.Flight;

/**
 * Created by grazi on 11.05.16.
 */
public abstract class FlightFactory {

    protected int flightNumber;

    public Flight buildANewAirplane(String flightType) {
        return newInstanceOfFlight(count(), flightType);
    }

    protected abstract int count ();

    protected abstract Flight newInstanceOfFlight(int flightNumber, String flightType);


}
