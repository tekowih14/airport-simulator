package factory;

import flight.CargoFlight;
import flight.Flight;

/**
 * Created by grazi on 11.05.16.
 */
public class CargoFlightFactory extends FlightFactory {

    protected Flight newInstanceOfFlight(int flightNumber, String flightType) {
        if ("A380".equals(flightType) || "dreamliner".equals(flightType)
                || "Dreamliner".equals(flightType)) {
            return new CargoFlight(flightNumber, flightType);
        }
        return null;
    }

    protected int count() {
        return --flightNumber;
    }


}
