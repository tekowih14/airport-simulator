package person;

/**
 * Created by grazi on 11.05.16.
 */
public class Person {
    private String name;

    public Person (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
