package praesenz.liste_beispiel;

import person.Pilot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 12.05.16.
 */
public class ListeBeispiel {

    public static void main(String[] args) {

        List<Pilot> pilots = new ArrayList<Pilot>();

        Pilot hans = new Pilot("Hans");
        Pilot fritz = new Pilot("Fritz");
        pilots.add(hans);
        pilots.add(fritz);

        pilots.size();

        pilots.remove(0);

        pilots.remove(fritz);
    }
}
