package app;

import airport.Airport;
import factory.CargoFlightFactory;
import factory.FlightFactory;
import factory.PassengerFlightFactory;
import person.CabinCrewMember;
import person.Passenger;
import person.Pilot;
import person.RandomNameGenerator;

import java.util.List;

/**
 * Created by grazi on 11.05.16.
 */
public class App {

    public static void main(String[] args) {

        FlightFactory passengerFlightFactory = new PassengerFlightFactory();
        FlightFactory cargoFlightFactory = new CargoFlightFactory();


        Airport zuerich = new Airport("Zürich", 5);

        zuerich.acceptNewBuildedFlight(passengerFlightFactory.buildANewAirplane("A380"));
        zuerich.acceptNewBuildedFlight(cargoFlightFactory.buildANewAirplane("Dreamliner"));

        for (int i = 0; i < 10; i++) {
            zuerich.acceptNewPilot(new Pilot(RandomNameGenerator.generateName()));
        }

        for (int i = 0; i < 40; i++) {
            zuerich.acceptNewCabinCrewMember(new CabinCrewMember(RandomNameGenerator.generateName()));
        }

        for (int i = 0; i < 300; i++) {
            zuerich.acceptNewPassenger(new Passenger(RandomNameGenerator.generateName()));
        }

        List<Integer> flightNumbers = zuerich.getFlightsWithoutFlightCrew();

        for (Integer flightNumber : flightNumbers) {
            zuerich.boardingFlightCrew(flightNumber);
        }

        flightNumbers = zuerich.getCargoFlights4LoadCargo();

        for (Integer flightNumber : flightNumbers) {
            zuerich.loadCargo4Flight(flightNumber);
        }

        flightNumbers = zuerich.getPassengerFlights4LoadPassenger();

        for (Integer flightNumber : flightNumbers) {
            zuerich.loadPassenger4Flight(flightNumber, 100);
        }

        Airport honululu = new Airport("Honululu", 4);

        flightNumbers = zuerich.getFlightsWithoutDestination();

        for (Integer flightNumber : flightNumbers) {
            zuerich.setDestination(flightNumber, honululu);
        }

        flightNumbers = zuerich.getFlights4TakeOff();

        for (Integer flightNumber : flightNumbers) {
            zuerich.takeOffFlight(flightNumber);
        }

        flightNumbers = honululu.getFlights4Landing();

        for (Integer flightNumber : flightNumbers) {
            honululu.landingFlight(flightNumber);
        }

        flightNumbers = honululu.getFlights4GoToGate();

        for (Integer flightNumber : flightNumbers) {
            honululu.unloadPassengers(flightNumber);
        }

        flightNumbers = honululu.getFlights4GoToUnloadCargo();

        for (Integer flightNumber : flightNumbers) {
            honululu.unloadCargo(flightNumber);
        }

        flightNumbers = honululu.getFlights4GoToUnloadFlightCrew();

        for (Integer flightNumber : flightNumbers) {
            honululu.unloadFlightCrew(flightNumber);
        }




    }

}
