package flight;

import person.Passenger;
import person.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 28.04.16.
 */
public class PassengerFlight extends Flight {

    public final static int PASSENGER_NOT_ON_BOARD = 7;
    public final static int PASSENGER_ON_BOARD = 4;

    protected List<Passenger> passengers = new ArrayList<Passenger>();

    protected int passengerStatus = PASSENGER_NOT_ON_BOARD;

    public PassengerFlight (int flightNumber, String flightType) {
        super (flightNumber, flightType);
    }

    public void boardingPassengers (List<Passenger> passengers) {
        if (status == CABIN_CREW_ON_BOARD) {
            passengerStatus = PASSENGER_ON_BOARD;
            this.passengers.addAll(passengers);
        }

    }

    public List<Person> unloadPassengers() {
        return null;
    }

    public int getPassengerStatus () {
        return passengerStatus;
    }
}
