package flight;

import airport.Airport;
import person.CabinCrewMember;
import person.Passenger;
import person.Person;
import person.Pilot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 28.04.16.
 */
public abstract class Flight {

    public final static int EMPTY_AIRPLANE = 0;
    public final static int CABIN_CREW_ON_BOARD = 1;
    public final static int AIRPLANE_ON_AIR = 5;
    public final static int AIRPLANE_LANDED = 6;


    protected int flightNumber;
    protected String flightType;

    private Airport destAirport;

    private List<CabinCrewMember> members = new ArrayList<CabinCrewMember>();
    private List<Pilot> pilots = new ArrayList<Pilot>();
    private List<Passenger> passengers = new ArrayList<Passenger>();

    protected int status = EMPTY_AIRPLANE;


    public Flight (int flightNumber, String flightType) {
        super();
        this.flightNumber = flightNumber;
        this.flightType = flightType;
    }

    public void setDestAirport(Airport destAirport) {
        this.destAirport = destAirport;
    }

    public Airport getDestAirport() {
        return destAirport;
    }

    public void boardingFlightCrew(List<Person> persons) {

        for (Person person : persons) {
            if (person instanceof CabinCrewMember) {
                members.add((CabinCrewMember) person);
            } else if (person instanceof Pilot) {
                pilots.add((Pilot) person);
            }
        }

        if (members.size() > 4) {
            status = CABIN_CREW_ON_BOARD;
        } else {
            members.clear();
            pilots.clear();

        }

    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public String getFlightType() {
        return flightType;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Flight) {
            Flight f = (Flight) obj;

            if (f.getFlightNumber() == this.flightNumber
                    && f.getFlightType().equals(this.flightType)) {
                return true;
            }

        } else {
            System.out.println("Sorry, kein Flugzeug");
            return false;
        }

        return false;
    }


    public void takeOff() {
        if (status == CABIN_CREW_ON_BOARD) {
            status = AIRPLANE_ON_AIR;
        }
    }

    public void land() {
        if (status == AIRPLANE_ON_AIR) {
            status = AIRPLANE_LANDED;
        }
    }

    public void unloadFlightCrew() {
        pilots.clear();
        members.clear();
        status = EMPTY_AIRPLANE;
    }


    List<Pilot> getPilots() {
        return pilots;
    }

    List<CabinCrewMember> getMembers() {
        return members;
    }
}
