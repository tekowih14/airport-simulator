package flight;

import fixtures.Fixtures;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static flight.CargoFlight.CARGO_LOADED;
import static flight.CargoFlight.CARGO_NOT_LOADED;
import static flight.Flight.CABIN_CREW_ON_BOARD;
import static flight.Flight.EMPTY_AIRPLANE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by grazi on 15.05.16.
 */
public class CargoFlightTest {

    @Test
    public void shouldAddCargo() throws Exception {

        CargoFlight cargoFlight = Fixtures.getCargoA380StatusWithCargoStatus(1, CABIN_CREW_ON_BOARD, CARGO_NOT_LOADED);

        cargoFlight.addCargo();

        assertThat(cargoFlight.getCargoStatus(), is(CARGO_LOADED));
        assertThat(cargoFlight.getStatus(), is(CABIN_CREW_ON_BOARD));

    }

    @Test
    public void shouldNotAddCargo_wrongStatus() throws Exception {

        CargoFlight cargoFlight = Fixtures.getCargoA380StatusWithCargoStatus(1, EMPTY_AIRPLANE, CARGO_NOT_LOADED);

        cargoFlight.addCargo();

        assertThat(cargoFlight.getCargoStatus(), is(CARGO_NOT_LOADED));
        assertThat(cargoFlight.getStatus(), is(EMPTY_AIRPLANE));
    }

}