package fixtures;

import flight.CargoFlight;

/**
 * Created by grazi on 15.05.16.
 */
public class CargoFlightStatus extends CargoFlight {

    public CargoFlightStatus(int flightNumber, String flightType, int status) {
        super(flightNumber, flightType);
        this.status = status;
    }


    public CargoFlightStatus(int flightNumber, String flightType, int status, int cargoStatus) {
        this (flightNumber, flightType, status);
        this.cargoStatus = cargoStatus;
    }
}
