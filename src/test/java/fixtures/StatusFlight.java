package fixtures;

import flight.PassengerFlight;
import person.Passenger;
import person.RandomNameGenerator;

/**
 * Created by grazi on 12.05.16.
 */
public class StatusFlight extends PassengerFlight {

    public StatusFlight(int flightNumber, String flightType, int status) {
        super(flightNumber, flightType);
        this.status = status;
    }

    public StatusFlight (int flightNumber, String flightType, int status, int anzPassengers) {
        this (flightNumber, flightType, status);
        for (int i = 0; i < anzPassengers; i++) {
            passengers.add(new Passenger(RandomNameGenerator.generateName()));
        }
        this.passengerStatus = PASSENGER_ON_BOARD;
    }
}
