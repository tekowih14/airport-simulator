package factory;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by grazi on 12.05.16.
 */
public class PassengerFlightFactoryTest {
    @Test
    public void shouldCreateNewInstanceOfFlight_A380() throws Exception {

        PassengerFlightFactory factory = new PassengerFlightFactory();

        Flight passengerFlight = factory.newInstanceOfFlight(1, "A380");

        assertThat(passengerFlight, is(notNullValue()));
        assertThat(passengerFlight, is(instanceOf(PassengerFlight.class)));
    }

    @Test
    public void shouldCreateNewInstanceOfFlight_Dreamliner() throws Exception {
        // Arrange
        PassengerFlightFactory factory = new PassengerFlightFactory();

        // Act
        Flight cargoFlight = factory.newInstanceOfFlight(1, "Dreamliner");

        // Assert
        assertThat(cargoFlight, is(notNullValue()));
        assertThat(cargoFlight, is(instanceOf(PassengerFlight.class)));
    }

    @Test
    public void shouldCreateNewInstanceOfFlight_Dreamliner_caseSensitive() throws Exception {

        PassengerFlightFactory factory = new PassengerFlightFactory();

        Flight cargoFlight = factory.newInstanceOfFlight(1, "dreamliner");

        assertThat(cargoFlight, is(notNullValue()));
        assertThat(cargoFlight, is(instanceOf(PassengerFlight.class)));
    }

    @Test
    public void shouldCreateNewInstanceOfFlight_not_Dreamliner_A380() throws Exception {
        // Arrange
        PassengerFlightFactory factory = new PassengerFlightFactory();

        //Act
        Flight cargoFlight = factory.newInstanceOfFlight(1, "Jumbo");

        // Assert
        assertThat(cargoFlight, is(nullValue()));
    }

}